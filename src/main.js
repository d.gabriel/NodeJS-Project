const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

// 1. import sesssion & passport
const session = require('express-session');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);

const mongodb = require('./config/database');
const utils = require('./utils');
const authRoutes = require('./apis/auth.api');

// 3. require config
require('./passport');

const main = async () => {
  await mongodb.connect();
  const app = express();
  app.use(bodyParser.urlencoded({
    extended: false,
  }));
  app.use(bodyParser.json());

  // 2. config passport
  const sessionMiddleWare = session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: (7 * 24 * 60 * 60 * 1000),
    },
    store: new MongoStore({
      db: mongodb.getDb(),
    }),
  });
  app.use(sessionMiddleWare);
  app.use(passport.initialize());
  app.use(passport.session());
  // done passport
  const login = (req, res, next) => {
    res.locals.login = req.isAuthenticated();
    next();
  };
  app.use(login);

  app.use('/public', express.static(path.join(utils.rootPath, 'public')));
  app.set('view engine', 'ejs');
  app.use('/', authRoutes);
  app.set('views', path.join(utils.rootPath, 'views'));


  const server = http.createServer(app);

  const port = +process.env.PORT || 8080;
  server.listen(port, '0.0.0.0', (err) => {
    if (err) {
      console.log('Create server got an error', err);
    } else {
      console.log('Server listening', port);
    }
  });
};

main();