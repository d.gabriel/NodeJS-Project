const { Router } = require('express');
const passport = require('passport');

const userRepo = require('../repository/user.mongo-repo');
const userService = require('../services/user.service');
const loginRequire = require('./middleware/authRequiredMiddleWare');

// Home pagee
const homePageController = (req, res) => {
  res.render('home', {
    title: 'Home Page',
  });
};

// Login
const loginFormController = (req, res) => {
  res.render('login', {
    title: 'Login',
  });
};

const loginController = (req, res) => {
  res.cookie('_token', req.user.token, {
    maxAge: 7 * 24 * 60 * 60 * 1000,
  });
  res.redirect('/');
};

// Register
const registerFormController = (req, res) => {
  res.render('register', { register: 0 });
};

const createUserController = async (req, res) => {
  try {
    const user = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      confirmpassword: req.body.confirmpassword,
    };
    const newUser = await userService.createUser(user);
    const isUser = await userRepo.findByUsername(newUser.username);
    if (isUser !== null) {
      res.redirect('/sign-in');
    } else {
      res.render('register', {
        title: 'Register',
      });
    }
  } catch (error) {
    res.json({ error: error.message });
  }
};

// Collect Infomation
const collectFormController = (req, res) => {
  res.render('collect-information', {
    title: 'Update Information',
    user: req.user,
  });
};

const updateUserInformationController = async (req, res) => {
  const userUpdate = {
    fname: req.body.Fname,
    lname: req.body.Lname,
    gender: req.body.Gender,
    height: req.body.Height,
    weight: req.body.Weight,
    age: req.body.Age,
    active: req.body.active,
  };
  const updatedUser = await userRepo.updateById(req.user._id, userUpdate);
  res.render('home');
};
// Profile
const userProfileController = (req, res) => {
  res.render('user-profile', {
    user: req.user,
    fname: req.user.fname,
    lname: req.user.lname,
    height: req.user.height,
    weight: req.user.weight,
    gender: req.user.gender,
    age: req.user.age,
    bmi: req.user.bmi,
    bstatus: req.user.bodyStatus,
  });
};

// Log out
const logoutController = (req, res) => {
  req.logout();
  res.redirect('/sign-in');
};
// BMI
const BMI = (req, res) => {
  res.render('bmi', {
    user: req.user,
    height: req.user.height,
    weight: req.user.weight,
  });
};

const updateUserBMIStatusController = async (req, res) => {
  const userBMIUpdate = {
    bmi: req.body.bmi,
    bodyStatus: req.body.bodyStatus,
  };
  const updateUserBMI = await userRepo.updateById(req.user._id, userBMIUpdate);
  res.render('home');
};

// Work out plan
const workOutPlanController = (req, res) => {
  const bmi = req.user.bmi;
  const gender = req.user.gender;
  if (gender === 'Male') {
    if (bmi < 18.5) {
      res.render('work-out-plan/work-out-plan-for-bulking');
    } else if (bmi >= 18.5 && bmi <= 25) {
      res.render('work-out-plan/work-out-plan-for-bulking');
    } else if (bmi > 25 && bmi <= 30) {
      res.render('work-out-plan/work-out-plan-for-cutting');
    } else if (bmi > 30) {
      res.render('work-out-plan/work-out-plan-for-cutting');
    }
  } else if (gender === 'Female') {
    res.render('work-out-plan/work-out-plan-for-female');
  }
};
// Diet plan
const dietPlanController = (req, res) => {
  const bmi = req.user.bmi;
  const gender = req.user.gender;
  if (gender === 'Male') {
    if (bmi < 18.5) {
      res.render('diet-plan/diet-plan-for-bulking');
    } else if (bmi >= 18.5 && bmi <= 25) {
      res.render('diet-plan/diet-plan-for-bulking');
    } else if (bmi > 25 && bmi <= 30) {
      res.render('diet-plan/diet-plan-for-cutting');
    } else if (bmi > 30) {
      res.render('diet-plan/diet-plan-for-cutting');
    }
  } else if (gender === 'Female') {
    res.render('diet-plan/diet-plan-for-female');
  }
};

const createAuthRoutes = () => {
  const authRoutes = Router();
  authRoutes.get('/', homePageController);
  // Log in
  authRoutes.get('/sign-in', loginFormController);
  authRoutes.post('/sign-in', passport.authenticate('local', { failureRedirect: '/sign-in' }), loginController);
  // Register
  authRoutes.get('/sign-up', registerFormController);
  authRoutes.post('/sign-up', createUserController);
  // Sign out
  authRoutes.get('/sign-out', logoutController);
  // Profile
  authRoutes.get('/profile', loginRequire, userProfileController);
  // Collect infomation
  authRoutes.get('/collect-information', loginRequire, collectFormController);
  authRoutes.post('/collect-information', loginRequire, updateUserInformationController);
  // BMI
  authRoutes.get('/bmi', loginRequire, BMI);
  authRoutes.post('/bmi', loginRequire, updateUserBMIStatusController);
  // Workout plan
  authRoutes.get('/work-out-plan', loginRequire, workOutPlanController);
  // Diet plan
  authRoutes.get('/diet-plan', loginRequire, dietPlanController);
  return authRoutes;
};

const authRoutes = createAuthRoutes();

module.exports = authRoutes;