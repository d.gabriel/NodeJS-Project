const express = require('express');
const userRepo = require('../repository/user.mongo-repo');
const userService = require('../services/user.service');

const findAllUsersController = async (req, res) => {
  const users = await userRepo.allUsers();
  res.json(users);
};

const createUsersController = async (req, res) => {
  try {
    const userForm = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      confirmpassword: req.body.confirmpassword,
    };
    console.log(userForm);
    const userInserted = await userService.createUser(userForm);
    console.log(userInserted);
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: err.message
    });
  }
};

const updateUserInfoController = async (req, res) => {
  try {
    const userUpdateForm = {
      fname: req.body.Fname,
      lname: req.body.Lname,
      gender: req.body.Gender,
      Height: req.body.Height,
      Weight: req.body.Weight,
    };
    console.log(userUpdateForm);
    const userUpdated = await userService.updateUser(userId, userUpdateForm);
    console.log(userUpdated);
  } catch (err) {
    res.status(400).json({
      error: err.message
    });
  }
};


module.exports = {
  findAllUsersController,
  createUsersController,
  updateUserInfoController
}